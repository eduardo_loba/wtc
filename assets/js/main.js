//efeito parallax em elementos especificos com a class "parallax"
$.fn.sparallax = function (options) {
    var $window = $(window),
            defaults = {
                $elements: $(this),
                speed: 4
            },
    settings = $.extend(defaults, options);
    $.each(settings.$elements, function (i, el) {

        var $el = $(el),
            offset = 0,
            speed = ($el.data('parallax-speed') ? $el.data('parallax-speed') : defaults.speed);
        var sector=$("#"+$el.data("sector"));

        //atribuir valores ONLOAD para nao ficar a zeros

        offset = -(($window.scrollTop()-sector.position().top) / speed);
        $el.css({top: offset + 'px'});

        //recalcular os valores ao scroll
        $window.scroll(function () {
            if(sector.hasClass("visible")){
                offset = -(($window.scrollTop()-sector.position().top) / speed);
                $el.css({top: offset + 'px'});
            }
        });
    });
};

function callPlayer(frame_id, func, args) {
    //if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;
    var iframe = document.getElementById(frame_id);
    if (iframe && iframe.tagName.toUpperCase() != 'IFRAME') {
        iframe = iframe.getElementsByTagName('iframe')[0];
    }
    if (iframe && iframe.contentWindow) {
        // When a function is supplied, just call it (like "onYouTubePlayerReady")
        if (func.call) return func();
        // Frame exists, send message
        iframe.contentWindow.postMessage(JSON.stringify({
            "event": "command",
            "func": func,
            "args": args || [],
            "id": frame_id
        }), "*");
    }
    /* IE8 does not support addEventListener... */
    function messageEvent(add, listener) {
        var w3 = add ? window.addEventListener : window.removeEventListener;
        w3 ?
            w3('message', listener, !1)
        :
            (add ? window.attachEvent : window.detachEvent)('onmessage', listener);
    }
}

var s,pos,stickermax;

$(document).ready(function () {

    $(".parallax").sparallax();

    if($(".video").length){
        // posicionar o menuHamburguer fixed na sector-video quando faz scroll
        s = $("#sector-video");
        pos = s.position();
        stickermax = pos.top + s.outerHeight() - $("#sector-home").height(); //40 value is the total of the top and bottom margin
        //fim

        //fitvideo
        $(".video").fitVids();
        //fim

        //open video & play
        $(".video").click(function(){
            $(".video").addClass("big");
            callPlayer("sector-video","playVideo");
        });
        //fim
    }

    $(".image_content").click(function(){
        if($(this).data("id")){
            $("#detail-image-"+$(this).data("id")).addClass("open");

            //posicionar na div se não está
            if(!$("#subContentFotos").hasClass("visible")){
                $('html, body').animate({scrollTop: $("#subContentFotos").offset().top}, 1);
            }

            //fechar o menu se foi aberto a partir de lá
            $("#menuLateral").removeClass("open");
        }

        return false;
    });
    $(".detail_content .close").click(function(){
        $(this).parent().removeClass("open");
        return false;
    });

    $(".menuHamburguer").click(function(){
        $("#menuLateral").toggleClass("open");
    });

    $("#menuLateral .close").click(function(){
        $("#menuLateral").removeClass("open");
    });

    sectorVisible();
});


$(window).scroll(function() {
    // posicionar o menuHamburguer fixed na sector-video quando faz scroll
    var windowpos = $(window).scrollTop();
    if (windowpos >= pos.top && windowpos < stickermax) {
        s.find(".menuHamburguer").css({top: (windowpos-pos.top+70) + "px"}); //set sticker right above the footer
    }
    //fim

    sectorVisible();
});

$(window).load(function () {
    sectorVisible();
});

//adicionar class 'visible' ao sector visivel
function sectorVisible() {
    if ($('#mobileVisible').css('display') == ('none')) { //DESKTOP
        var sectors = $('#sector-home, #sector-video,#subContentFotos,#sector-join,#responsability,#ngo,#sector-partners'),
            menu = $('#main_menu li'),
            currentPosition = $(window).scrollTop() + ($(window).height() / 2);

        sectors.each(function () {
            var top = $(this).offset().top,
                bottom = top + $(this).outerHeight();

            if (currentPosition > top && currentPosition < bottom) {
                menu.removeClass('selected');
                sectors.removeClass('visible');

                //close video & pause
                if($(this).attr("id")!='sector-video' && $(".video").hasClass("big")){
                    $(".video").removeClass("big");
                    callPlayer("sector-video","pauseVideo");
                }

                $(this).addClass('visible');
                //adicionar a class "selected" ao "li" do main menu
                menu.find('a[href="#' + $(this).attr('id') + '"]').parent().addClass('selected');
            }
        });
        //fim

    }
}
